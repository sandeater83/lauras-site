/**
 * Created by stuartadams on 5/12/15.
 */

var http = require("http");

//print out message
function printMessage(username, badgeCount, JSPoints)
{
    console.log(username + " has " + badgeCount + " badges and " + JSPoints + " JavaScript points");
}

//print out error messages

function printError(error)
{
    console.error(error.message);
}

function get(username)
{

    var request = http.get("http://teamtreehouse.com/" + username +".json", function(response)
    {
        var body = "";
        response.on("data", function(chunk){
            body += chunk;
        });

        response.on("end", function()
        {
            if(response.statusCode == 200)
            {
                try
                {
                    var profileJSON = JSON.parse(body);
                    printMessage(username, profileJSON.badges.length, profileJSON.points.JavaScript);
                }
                catch (error)
                {
                    //Prase error
                    console.log("Parse error: ");
                    printError(error);
                }
            }
            else
            {
                printError({message:"There was an error getting the profile for " + username + ". ("
                + http.STATUS_CODES[response.statusCode]+ ")"});
            }
        });
    });

    request.on("error", printError);
}

module.exports.get = get;