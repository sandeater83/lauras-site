/**
 * Created by stuartadams on 5/12/15.
 */


//Create web server

var http = require("http");
var router = require("./router.js");

http.createServer(function(request, response){
    router.home(request, response);
    router.user(request, response);
}).listen(3440, '127.0.0.1');

console.log('Server running at http://127.0.0.1:3440/');


//Function that handles the reading of files and merge in values